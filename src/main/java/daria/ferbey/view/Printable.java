package daria.ferbey.view;

public interface Printable {
    void print();
}
