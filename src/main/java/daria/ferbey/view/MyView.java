package daria.ferbey.view;

import daria.ferbey.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Logger logger = LogManager.getLogger(MyView.class);
    private Controller controller = new Controller();
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private Scanner input = new Scanner(System.in);

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Test serializable");
        menu.put("2", "2 - Test deserializable");
        menu.put("Q", "Q - exit ");

    }

    public MyView() {
        methodMenu = new LinkedHashMap<>();

        setMenu();
        methodMenu.put("1", this::task1Serializable);
        methodMenu.put("2", this::task1Deserializable);
    }

    private void task1Serializable() {
        controller.testDroiSer();
    }
    private void task1Deserializable() {
        controller.testDroidDes();
    }

    private void task2() {
    }

    //--------------------------------------------------------------------
    private void outputMenu() {
        System.out.println("\nMenu");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String menuKey;
        do {
            outputMenu();
            System.out.print("Please, select menu point: ");
            menuKey = input.nextLine().toUpperCase();
            try {
                methodMenu.get(menuKey).print();
            } catch (Exception e) {
            }
        } while (!menuKey.equals("Q"));
    }
}
