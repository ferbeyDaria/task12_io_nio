package daria.ferbey.model;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Ship implements Serializable {
    private List<Droid> shipOfDroids;

    public Ship() {
        shipOfDroids = new ArrayList<>();
        shipOfDroids.add(new Droid("First",12,10,100));
        shipOfDroids.add(new Droid("Second",1,1,10));
        shipOfDroids.add(new Droid("Third",8,20,45));
    }

    public void testSerializable() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("ship.ser"));
            out.writeObject(shipOfDroids);
            out.close();
            System.out.println("Serialized date is saved");
        }catch (IOException i) {
            i.printStackTrace();
        }
    }

    public void testDeserialiazable() {
        List<Droid> e = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("ship.ser"));
            e= (List<Droid>) in.readObject();
            in.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        } catch (ClassNotFoundException ex) {
            System.out.println("Droid class not found");
            ex.printStackTrace();
            return;
        }
        System.out.println("Deserialiazed ship of droids...");
        for(int i=0; i<e.size();i++){
            System.out.println("Name : " + e.get(i).getName());
            System.out.println("Amount of armor : " + e.get(i).getArmorAmount());
            System.out.println("Amount of weapons : " + e.get(i).getWeaponsAmount());
            System.out.println("Power of weapons : " + e.get(i).getWeaponsPower());
        }

    }

    public List<Droid> getShipOfDroids() {
        return shipOfDroids;
    }
}
