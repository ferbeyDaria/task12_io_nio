package daria.ferbey.model;

import java.io.Serializable;

public class Droid implements Serializable {
    private String name;
    private int armorAmount;
    private transient int weaponsAmount;
    private int weaponsPower;

    public Droid(String name, int armorAmount, int weaponsAmount, int weaponsPower) {
        this.name = name;
        this.armorAmount = armorAmount;
        this.weaponsAmount = weaponsAmount;
        this.weaponsPower = weaponsPower;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getArmorAmount() {
        return armorAmount;
    }

    public void setArmorAmount(int armorAmount) {
        this.armorAmount = armorAmount;
    }

    public int getWeaponsAmount() {
        return weaponsAmount;
    }

    public void setWeaponsAmount(int weaponsAmount) {
        this.weaponsAmount = weaponsAmount;
    }

    public int getWeaponsPower() {
        return weaponsPower;
    }

    public void setWeaponsPower(int weaponsPower) {
        this.weaponsPower = weaponsPower;
    }

    @Override
    public String toString() {
        return  "Name='" + name + '\n' +
                "Amount of armor=" + armorAmount + '\n'+
                "Amount of weapons=" + weaponsAmount + '\n'+
                "Power of weapons=" + weaponsPower;
    }
}
